import CleanTerminal from 'clean-terminal-webpack-plugin';
import Dotenv from 'dotenv-webpack';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import path from 'path';
import webpack from 'webpack';
import nodeExternals from 'webpack-node-externals';
import WebpackBar from 'webpackbar';
import RunNodeWebpackPlugin from './node_modules/run-node-webpack-plugin/dist';

const babelLoader = {
	loader: 'babel-loader',
	options: {
		cacheDirectory: true,
		presets: ['@babel/preset-env']
	}
};

const config: webpack.Configuration = {
	devtool: 'eval-source-map',

	entry: ['@babel/polyfill', './src/index.ts'],

	externals: [nodeExternals()],

	mode: 'development',

	module: {
		rules: [
			{
				exclude: /node_modules/,
				test: /.tsx?$/,
				use: [
					{ loader: 'cache-loader' },
					{
						loader: 'thread-loader',
						options: {
							workers: require('os').cpus().length - 1
						}
					},
					babelLoader,
					{
						loader: 'ts-loader',
						options: {
							happyPackMode: true
						}
					}
				]
			},
			{
				exclude: /node_modules/,
				test: /\.graphql$/,
				use: [{ loader: 'graphql-import-loader' }]
			}
		]
	},

	optimization: {
		removeAvailableModules: false,
		removeEmptyChunks: false,
		splitChunks: false
	},

	output: {
		filename: 'index.js',
		path: path.join(__dirname, './dist')
	},

	plugins: [
		new CleanTerminal(),
		new ForkTsCheckerWebpackPlugin({
			checkSyntacticErrors: true,
			compilerOptions: {
				skipLibCheck: true,
				suppressOutputPathCheck: true
			},
			silent: true,
			tsconfig: './tsconfig.json',
			tslint: './tslint.json'
		}),

		new Dotenv(),
		new RunNodeWebpackPlugin(),
		new WebpackBar({
			color: '#3D55D1',
			name: 'Dashie',
			profile: true
		})
	],

	stats: {
		assets: false,
		children: false,
		chunks: false,
		colors: true,
		errorDetails: false,
		errors: true,
		hash: false,
		modules: false,
		publicPath: false,
		reasons: false,
		source: false,
		timings: false,
		version: false,
		warnings: false
	},

	resolve: {
		extensions: ['.tsx', '.ts', '.js']
	},

	target: 'node',

	watch: true
};

export default config;
