declare module '*.gql' {
	import { DocumentNode } from 'graphql';

	const content: DocumentNode;
	export default content;
}

declare module '*.graphql' {
	import { DocumentNode } from 'graphql';

	const content: DocumentNode;
	export default content;
}

declare module 'clean-terminal-webpack-plugin' {
	interface IOptions {
		message?: string;
		onlyInWatchMode?: boolean;
	}

	class CleanTerminalPlugin {
		message: string;
		onlyInWatchMode: boolean;
		constructor(options?: IOptions);
		apply: (compiler: any) => void;
		shouldClearConsole: (compiler: any) => any;
		clearConsole: () => void;
	}

	export default CleanTerminalPlugin;
}
