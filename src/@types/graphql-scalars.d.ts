declare module '@okgrow/graphql-scalars' {
	import { GraphQLScalarType } from 'graphql';

	export const DateTime: GraphQLScalarType;
	export const PositiveInt: GraphQLScalarType;
	export const NonNegativeInt: GraphQLScalarType;
	export const UnsignedInt: GraphQLScalarType;
	export const NegativeInt: GraphQLScalarType;
	export const PositiveFloat: GraphQLScalarType;
	export const NonNegativeFloat: GraphQLScalarType;
	export const UnsignedFloat: GraphQLScalarType;
	export const NegativeFloat: GraphQLScalarType;
	export const EmailAddress: GraphQLScalarType;
	export const URL: GraphQLScalarType;
	export const PhoneNumber: GraphQLScalarType;
	export const PostalCode: GraphQLScalarType;
	export const RegularExpression: GraphQLScalarType;

}
