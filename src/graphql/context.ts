import { Request } from 'express-serve-static-core';

/* tslint:disable:interface-name */
export interface User {
	id: string;
	name: string;
	email: any;
}

/* tslint:disable:interface-name */
export interface AuthPayload {
	token: string;
	user: User;
}

/* tslint:disable:interface-name */
export interface Context {
	// prisma: Prisma;
	req: Request;
}
