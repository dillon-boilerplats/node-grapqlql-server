import { AuthPayloadResolvers } from './graphqlgen';

/* tslint:disable:variable-name */
export const AuthPayload: AuthPayloadResolvers.Type = {
	...AuthPayloadResolvers.defaultResolvers
};
