import { UserResolvers } from './graphqlgen';

/* tslint:disable:variable-name */
export const User: UserResolvers.Type = {
	...UserResolvers.defaultResolvers
};
