import { QueryResolvers } from './graphqlgen';

/* tslint:disable:variable-name */
export const Query: QueryResolvers.Type = {
	...QueryResolvers.defaultResolvers,
	hello: (parent, args, ctx) => {
		return `Hello ${args.name}`;
	},
	users: (parent, args, ctx) => {
		throw new Error('Resolver not implemented');
	}
};
