// Code generated by github.com/prisma/graphqlgen, DO NOT EDIT.

import { GraphQLResolveInfo } from 'graphql';
import { User, AuthPayload, Context } from '../context';

export namespace QueryResolvers {
	export const defaultResolvers = {};

	export interface ArgsHello {
		name: string;
	}

	export type HelloResolver = (
		parent: undefined,
		args: ArgsHello,
		ctx: Context,
		info: GraphQLResolveInfo
	) => string | null | Promise<string | null>;

	export type UsersResolver = (
		parent: undefined,
		args: {},
		ctx: Context,
		info: GraphQLResolveInfo
	) => Array<User | null> | null | Promise<Array<User | null> | null>;

	export interface Type {
		hello: (
			parent: undefined,
			args: ArgsHello,
			ctx: Context,
			info: GraphQLResolveInfo
		) => string | null | Promise<string | null>;

		users: (
			parent: undefined,
			args: {},
			ctx: Context,
			info: GraphQLResolveInfo
		) => Array<User | null> | null | Promise<Array<User | null> | null>;
	}
}

export namespace UserResolvers {
	export const defaultResolvers = {
		id: (parent: User) => parent.id,
		name: (parent: User) => parent.name,
		email: (parent: User) => parent.email
	};

	export type IdResolver = (
		parent: User,
		args: {},
		ctx: Context,
		info: GraphQLResolveInfo
	) => string | Promise<string>;

	export type NameResolver = (
		parent: User,
		args: {},
		ctx: Context,
		info: GraphQLResolveInfo
	) => string | Promise<string>;

	export type EmailResolver = (
		parent: User,
		args: {},
		ctx: Context,
		info: GraphQLResolveInfo
	) => string | Promise<string>;

	export interface Type {
		id: (
			parent: User,
			args: {},
			ctx: Context,
			info: GraphQLResolveInfo
		) => string | Promise<string>;

		name: (
			parent: User,
			args: {},
			ctx: Context,
			info: GraphQLResolveInfo
		) => string | Promise<string>;

		email: (
			parent: User,
			args: {},
			ctx: Context,
			info: GraphQLResolveInfo
		) => string | Promise<string>;
	}
}

export namespace MutationResolvers {
	export const defaultResolvers = {};

	export interface ArgsCreateUser {
		name: string;
		email: string;
	}

	export interface ArgsLoginUser {
		email: string;
	}

	export type CreateUserResolver = (
		parent: undefined,
		args: ArgsCreateUser,
		ctx: Context,
		info: GraphQLResolveInfo
	) => AuthPayload | null | Promise<AuthPayload | null>;

	export type LoginUserResolver = (
		parent: undefined,
		args: ArgsLoginUser,
		ctx: Context,
		info: GraphQLResolveInfo
	) => AuthPayload | null | Promise<AuthPayload | null>;

	export interface Type {
		createUser: (
			parent: undefined,
			args: ArgsCreateUser,
			ctx: Context,
			info: GraphQLResolveInfo
		) => AuthPayload | null | Promise<AuthPayload | null>;

		loginUser: (
			parent: undefined,
			args: ArgsLoginUser,
			ctx: Context,
			info: GraphQLResolveInfo
		) => AuthPayload | null | Promise<AuthPayload | null>;
	}
}

export namespace AuthPayloadResolvers {
	export const defaultResolvers = {
		token: (parent: AuthPayload) => parent.token,
		user: (parent: AuthPayload) => parent.user
	};

	export type TokenResolver = (
		parent: AuthPayload,
		args: {},
		ctx: Context,
		info: GraphQLResolveInfo
	) => string | Promise<string>;

	export type UserResolver = (
		parent: AuthPayload,
		args: {},
		ctx: Context,
		info: GraphQLResolveInfo
	) => User | Promise<User>;

	export interface Type {
		token: (
			parent: AuthPayload,
			args: {},
			ctx: Context,
			info: GraphQLResolveInfo
		) => string | Promise<string>;

		user: (
			parent: AuthPayload,
			args: {},
			ctx: Context,
			info: GraphQLResolveInfo
		) => User | Promise<User>;
	}
}

export interface Resolvers {
	Query: QueryResolvers.Type;
	User: UserResolvers.Type;
	Mutation: MutationResolvers.Type;
	AuthPayload: AuthPayloadResolvers.Type;
}
