import { MutationResolvers } from './graphqlgen';

/* tslint:disable:variable-name */
export const Mutation: MutationResolvers.Type = {
	...MutationResolvers.defaultResolvers,
	createUser: (parent, args, ctx) => {
		throw new Error('Resolver not implemented');
	},
	loginUser: (parent, args, ctx) => {
		throw new Error('Resolver not implemented');
	}
};
