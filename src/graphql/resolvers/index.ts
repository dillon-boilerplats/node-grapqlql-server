import Scalars from '@okgrow/graphql-scalars';
import { GraphQLScalarType } from 'graphql';
import GraphQLJSON from 'graphql-type-json';
import { AuthPayload } from './AuthPayload';
import { Resolvers } from './graphqlgen';
import { Mutation } from './Mutation';
import { Query } from './Query';
import { User } from './User';

interface IScalars {
	DateTime: GraphQLScalarType;
	PositiveInt: GraphQLScalarType;
	NonNegativeInt: GraphQLScalarType;
	UnsignedInt: GraphQLScalarType;
	NegativeInt: GraphQLScalarType;
	PositiveFloat: GraphQLScalarType;
	NonNegativeFloat: GraphQLScalarType;
	UnsignedFloat: GraphQLScalarType;
	NegativeFloat: GraphQLScalarType;
	EmailAddress: GraphQLScalarType;
	URL: GraphQLScalarType;
	PhoneNumber: GraphQLScalarType;
	PostalCode: GraphQLScalarType;
	RegularExpression: GraphQLScalarType;
}

interface IResolvers extends Resolvers, IScalars {
	[key: string]: any;
	JSON: GraphQLScalarType;
}

export const resolvers: IResolvers = {
	AuthPayload,
	Mutation,
	Query,
	User,
	...Scalars,
	JSON: GraphQLJSON
};
