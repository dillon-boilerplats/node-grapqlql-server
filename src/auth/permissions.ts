import { and, not, or, rule, shield } from 'graphql-shield';
import { getUserId } from './auth';

const isAuthenticated = rule()(async (parent, args, ctx, info) => {
	return getUserId(ctx) !== undefined;
});

const permissions = shield({
	Query: {
		hello: not(isAuthenticated),
		users: isAuthenticated
	}
});

export default permissions;
