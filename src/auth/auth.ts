import bcrypt from 'bcryptjs';
import jwt, { verify } from 'jsonwebtoken';
import { Context } from '../graphql/context';

// import { Prisma } from '../generated/prisma-client';

export const generateAuthToken = (userId: string): string => {
	const expirationTime = parseInt(process.env.JWT_EXPIRATION as string, 10);

	const token = jwt
		.sign(
			{
				id: userId.toString()
			},
			process.env.APP_SECRET as string,
			{ expiresIn: `${expirationTime}s` }
		)
		.toString();

	return token;
};

export const APP_SECRET = process.env.APP_SECRET;

export interface IToken {
	id: string;
}

export const getUserId = (context: Context): string | undefined => {
	const authorization = context.req.get('Authorization');

	if (authorization) {
		const token = authorization.replace('Bearer ', '');

		const verifiedToken = verify(token, APP_SECRET as string) as IToken;

		return verifiedToken && verifiedToken.id;
	}
	return undefined;
};

export const hasher = async (password: string): Promise<string> => {
	const salt = await bcrypt.genSaltSync(10);
	const hashedPassword = await bcrypt.hash(password, salt);

	return hashedPassword;
};

export const verifyPassword = async (
	givenPassword: string,
	actualPassword: string
): Promise<boolean> => {
	const valid: boolean = await bcrypt.compare(givenPassword, actualPassword);
	return valid;
};
