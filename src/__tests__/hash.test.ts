import { hasher } from '../auth/auth';

describe('Password hashing', () => {
	test('hashes password', async () => {
		const password = 'Password';

        const hashedPassword = await hasher(password);
        
        expect(hashedPassword).toBeDefined()
        expect(hashedPassword).not.toEqual(password)
	});
});
