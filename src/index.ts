import { GraphQLServer } from 'graphql-yoga';
import path from 'path';
import permissions from './auth/permissions';
import { resolvers } from './graphql/resolvers';

const server = new GraphQLServer({
	resolvers,
	middlewares: [permissions],
	typeDefs: path.join('./src/graphql/typeDefs.graphql')
});

server.start(() => console.log('Server is running on localhost:4000'));
